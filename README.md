# Hostelworld test app

### About

Some technologies|patterns used to build this app are:

- Mpv pattern.
- DI (no external library).
- RxJava and RxAndroid to manage streams of data.
- Retrofit and HttpOk for network requests.
- Room as an abstraction layer over SQLite.
- Repository pattern.
- pmd, findbugs, checkstyle to verify the code quality.

### Develoment enviroment

- Android Studio 3.1
- Gradle for android 3.1

### Notes

- Although it has some Kotlin code it wasn't written all in Kotlin most of the code is Java code.

### Screenshots

![alt text](screenshots/1.jpg)
![alt text](screenshots/2.jpg)
![alt text](screenshots/3.jpg)
