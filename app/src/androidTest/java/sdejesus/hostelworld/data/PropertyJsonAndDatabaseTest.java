package sdejesus.hostelworld.data;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.google.gson.Gson;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import sdejesus.hostelworld.data.local.database.HostelDatabase;
import sdejesus.hostelworld.data.model.Property;
import sdejesus.hostelworld.network.reponse.PropertiesResponse;

import static sdejesus.hostelworld.util.Files.inputStreamToString;

@RunWith(AndroidJUnit4.class)
public class PropertyJsonAndDatabaseTest {

    private HostelDatabase mDatabase;
    private List<Property> mProperties;

    @Rule
    public InstantTaskExecutorRule mInstantTaskExecutorRule =
            new InstantTaskExecutorRule();
    @Before
    public void initDbAndJson() throws Exception {
        mDatabase = Room.inMemoryDatabaseBuilder(
                InstrumentationRegistry.getContext(),
                HostelDatabase.class)
                .allowMainThreadQueries() // for testing
                .build();
        ClassLoader classLoader = getClass().getClassLoader();
        String json = inputStreamToString(classLoader.getResourceAsStream(("property.json")));
        PropertiesResponse propertiesFromJson = new Gson().fromJson(
                json,
                PropertiesResponse.class
        );
        mProperties = propertiesFromJson.getProperties();
    }

    @After
    public void closeDb() throws Exception {
        mDatabase.close();
    }

    @Test
    public void jsonLoader() {
        Assert.assertTrue(mProperties.size() > 0);
    }

    @Test
    public void insertAndGetPropertyById() {

        Property propertyTest = mProperties.get(0);
        mDatabase.propertyDao().saveProperty(propertyTest);
        mDatabase.propertyDao()
                .loadProperty(propertyTest.getId())
                .test()
                .assertValue(property -> property.getId() == propertyTest.getId() &&
                        property.getName().equals(propertyTest.getName()));
    }
}

