package sdejesus.hostelworld.home;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import io.reactivex.Flowable;
import sdejesus.hostelworld.base.BaseSchedulerProvider;
import sdejesus.hostelworld.base.ImmediateScheduler;
import sdejesus.hostelworld.data.PropertyRepository;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MainPresenterTest {

    @Mock
    private PropertyRepository mPropertiesRepository;

    @Mock
    private MainContract.View mPropertiesView;

    private BaseSchedulerProvider mSchedulerProvider;

    private MainPresenter mPropertiesPresenter;

    @Before
    public void setupPropertiesPresenter() {
        MockitoAnnotations.initMocks(this);

        mSchedulerProvider = new ImmediateScheduler();
        mPropertiesPresenter = new MainPresenter(mPropertiesView, mPropertiesRepository, mSchedulerProvider);
    }

    @Test
    public void createPresenterSetsThePresenterToView() {
        // Get a reference to the class under test
        mPropertiesPresenter = new MainPresenter(mPropertiesView, mPropertiesRepository, mSchedulerProvider);
        // Then the presenter is set to the view
        verify(mPropertiesView).setPresenter(mPropertiesPresenter);
    }

    @Test
    public void loadAllPropertiesFromRepositoryVerifyLoadingIndicators() {

        when(mPropertiesRepository.getProperties()).thenReturn(Flowable.just(Collections.emptyList()));

        mPropertiesPresenter.load();

        verify(mPropertiesView).setLoadingIndicator(true);
        verify(mPropertiesView).setLoadingIndicator(false);
    }
}