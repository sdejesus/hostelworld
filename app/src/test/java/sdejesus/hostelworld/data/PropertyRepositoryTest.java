package sdejesus.hostelworld.data;

import android.content.Context;

import com.google.gson.Gson;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;
import sdejesus.hostelworld.data.model.Property;
import sdejesus.hostelworld.network.reponse.PropertiesResponse;

import static junit.framework.Assert.assertFalse;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static sdejesus.hostelworld.util.Files.inputStreamToString;

public class PropertyRepositoryTest {

    private PropertyRepository mPropertyRepository;

    @Mock
    private PropertyDataSource mPropertyRemoteDataSource;

    @Mock
    private PropertyDataSource mPropertyLocalDataSource;

    @Mock
    private CurrencyRepository mCurrencyRepository;

    @Mock
    private Context mContext;

    private List<Property> mProperties;

    @Before
    public void setupPropertyRepository() throws IOException {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        mPropertyRepository = PropertyRepository.getInstance(
                mPropertyLocalDataSource, mPropertyRemoteDataSource, mCurrencyRepository);

        ClassLoader classLoader = getClass().getClassLoader();
        String json = inputStreamToString(classLoader.getResourceAsStream(("property.json")));
        PropertiesResponse propertiesFromJson = new Gson().fromJson(
                json,
                PropertiesResponse.class
        );
        mProperties = propertiesFromJson.getProperties();
    }

    @After
    public void destroyRepositoryInstance() {
        mPropertyRepository.destroyInstance();
    }

    @Test
    public void getPropertiesRepositoryCachesAfterFirstSubscriptionWhenPropertiesAvailableInLocalStorage() {

        // set local data available
        setPropertiesAvailable(mPropertyLocalDataSource, mProperties);
        // set remote as not available
        setPropertiesNotAvailable(mPropertyRemoteDataSource);

        mPropertyRepository.forceCache(false);

        TestSubscriber<List<Property>> testSubscriber1 = new TestSubscriber<>();
        mPropertyRepository.getProperties().subscribe(testSubscriber1);

        TestSubscriber<List<Property>> testSubscriber2 = new TestSubscriber<>();
        mPropertyRepository.getProperties().subscribe(testSubscriber2);

        verify(mPropertyLocalDataSource).getProperties();
        verify(mPropertyRemoteDataSource).getProperties();
        //
        assertFalse(mPropertyRepository.mCacheIsDirty);
        testSubscriber1.assertValue(mProperties);
        testSubscriber2.assertValue(mProperties);
    }

    @Test
    public void getPropertiesRepositoryFromRemoteWhenLocalIsNotAvailable() {
        // set remote data available
        setPropertiesAvailable(mPropertyRemoteDataSource, mProperties);
        // set local as not available
        setPropertiesNotAvailable(mPropertyLocalDataSource);

        mPropertyRepository.forceCache(false);

        TestSubscriber<List<Property>> testSubscriber1 = new TestSubscriber<>();
        mPropertyRepository.getProperties().subscribe(testSubscriber1);

        TestSubscriber<List<Property>> testSubscriber2 = new TestSubscriber<>();
        mPropertyRepository.getProperties().subscribe(testSubscriber2);

        verify(mPropertyLocalDataSource).getProperties();
        verify(mPropertyRemoteDataSource).getProperties();

        assertFalse(mPropertyRepository.mCacheIsDirty);
        testSubscriber1.assertValue(mProperties);
        testSubscriber2.assertValue(mProperties);
    }

    @Test
    public void savePropertyLocalStorage() {

        Property property = mProperties.get(0);

        mPropertyRepository.saveProperty(property);

        // check that has been called on the local storage
        verify(mPropertyLocalDataSource).saveProperty(property);
        // Verify it was added to cached properties
        assertThat(mPropertyRepository.mCachedProperties.size(), is(1));
    }

    private void setPropertiesNotAvailable(PropertyDataSource dataSource) {
        when(dataSource.getProperties()).thenReturn(Flowable.just(Collections.emptyList()));
    }

    private void setPropertiesAvailable(PropertyDataSource dataSource, List<Property> properties) {
        when(dataSource.getProperties()).thenReturn(Flowable.just(properties).concatWith(Flowable.never()));
    }
}
