package sdejesus.hostelworld.data;

import com.google.gson.Gson;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import sdejesus.hostelworld.data.model.Facility;
import sdejesus.hostelworld.data.model.Image;
import sdejesus.hostelworld.data.model.Price;
import sdejesus.hostelworld.data.model.Property;
import sdejesus.hostelworld.network.reponse.FacilitiesResponse;
import sdejesus.hostelworld.network.reponse.ImagesResponse;
import sdejesus.hostelworld.network.reponse.LowestPricePerNightResponse;
import sdejesus.hostelworld.network.reponse.PropertiesResponse;

import static sdejesus.hostelworld.util.Files.inputStreamToString;

public class ModelTest {

    @Test
    public void testPropertiesJson() throws IOException {
        List<Property> properties;
        String json = getFile("property.json");
        PropertiesResponse propertiesFromJson = new Gson().fromJson(
                json,
                PropertiesResponse.class
        );
        properties = propertiesFromJson.getProperties();
        Assert.assertTrue(properties.size() > 0);
    }

    @Test
    public void testImagesJson() throws IOException {
        List<Image> images;
        String json = getFile("images.json");
        ImagesResponse imagesFromJson = new Gson().fromJson(
                json,
                ImagesResponse.class
        );
        images = imagesFromJson.getImages();
        Assert.assertTrue(images.size() > 0);
    }

    @Test
    public void testLowestPriceJson() throws IOException {
        Price price;
        String json = getFile("lowestpricepernight.json");
        LowestPricePerNightResponse lowestPricePerNightFromJson = new Gson().fromJson(
                json,
                LowestPricePerNightResponse.class
        );
        price = lowestPricePerNightFromJson.getPrice();
        Assert.assertTrue(price.getValue().equalsIgnoreCase("274.64"));
    }

    @Test
    public void testFacilitiesJson() throws IOException {
        List<Facility> facilities;
        String json = getFile("facilities.json");
        FacilitiesResponse facilitiesFromJson = new Gson().fromJson(
                json,
                FacilitiesResponse.class
        );
        facilities = facilitiesFromJson.getFacilities();
        Assert.assertTrue(facilities.size() > 0);
        int totalCategories = 0;
        for (Facility facility : facilities) { // could be flatmap to get internal sub
            if (facility.isCategory()) totalCategories = totalCategories + 1;
        }
        Assert.assertTrue(totalCategories == 5);
    }

    private String getFile(String name) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String json = inputStreamToString(classLoader.getResourceAsStream(name));
        return json;
    }
}
