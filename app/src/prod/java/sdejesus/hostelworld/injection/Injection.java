package sdejesus.hostelworld.injection;

import android.content.Context;
import android.support.annotation.NonNull;

import sdejesus.hostelworld.base.Scheduler;
import sdejesus.hostelworld.data.CurrencyDataSource;
import sdejesus.hostelworld.data.CurrencyRepository;
import sdejesus.hostelworld.data.PropertyDataSource;
import sdejesus.hostelworld.data.PropertyRepository;
import sdejesus.hostelworld.data.local.CurrencyLocalDataSource;
import sdejesus.hostelworld.data.local.PropertyLocalDataSource;
import sdejesus.hostelworld.data.local.dao.PropertyDao;
import sdejesus.hostelworld.data.local.database.HostelDatabase;
import sdejesus.hostelworld.data.remote.PropertyRemoteDataSource;
import sdejesus.hostelworld.network.NetworkApi;

import static com.google.common.base.Preconditions.checkNotNull;

public class Injection {

    // Just to prevent initialization
    private Injection() {

    }

    public static Scheduler provideScheduler() {
        return Scheduler.getInstance();
    }

    public static NetworkApi provideNetworkApi() {
        return NetworkApi.getInstance();
    }

    public static HostelDatabase provideDatabase(@NonNull Context context) {
        checkNotNull(context, "Context cannot be null");
        return HostelDatabase.getInstance(context);
    }

    public static PropertyDao providePropertyDatabase(@NonNull Context context) {
        checkNotNull(context, "Context cannot be null");
        return provideDatabase(context).propertyDao();
    }

    public static PropertyRepository providePropertyRepository(@NonNull Context context) {
        checkNotNull(context, "Context cannot be null");
        return PropertyRepository.getInstance(
                providePropertyLocalDataSource(context),
                providePropertyRemoteDataSource(),
                provideCurrencyRepository());
    }

    public static PropertyDataSource providePropertyLocalDataSource(@NonNull Context context) {
        checkNotNull(context, "Context cannot be null");
        return PropertyLocalDataSource.getInstance(providePropertyDatabase(context));
    }

    public static PropertyDataSource providePropertyRemoteDataSource() {
        return PropertyRemoteDataSource.getInstance(provideNetworkApi());
    }

    public static CurrencyRepository provideCurrencyRepository() {
        return CurrencyRepository.getInstance(provideCurrencyLocalDataSource());
    }

    public static CurrencyDataSource provideCurrencyLocalDataSource() {
        return CurrencyLocalDataSource.getInstance();
    }

}
