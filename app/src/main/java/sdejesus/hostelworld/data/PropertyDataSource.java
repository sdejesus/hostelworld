package sdejesus.hostelworld.data;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import sdejesus.hostelworld.data.model.Property;

public interface PropertyDataSource {

    Flowable<List<Property>> getProperties();

    Single<Property> getProperty(long id);

    void saveProperty(Property property);

    void forceCache(boolean value);

    void destroyInstance();
}
