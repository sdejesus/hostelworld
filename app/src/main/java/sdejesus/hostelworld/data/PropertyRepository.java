package sdejesus.hostelworld.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.Single;
import sdejesus.hostelworld.data.model.Property;

import static com.google.common.base.Preconditions.checkNotNull;
import static sdejesus.hostelworld.data.model.Currency.BASE_CURRENCY;

public class PropertyRepository implements PropertyDataSource {

    private static PropertyRepository mInstance;

    private PropertyDataSource mRemoteDataSource;
    private PropertyDataSource mLocalDataSource;

    private CurrencyRepository mCurrencyRepository;

    @Nullable
    public Map<Long, Property> mCachedProperties;

    @VisibleForTesting
    public boolean mCacheIsDirty;


    private PropertyRepository(@NonNull PropertyDataSource localDataSource,
                               @NonNull PropertyDataSource remoteDataSource,
                               @NonNull CurrencyRepository currencyRepository) {
        mLocalDataSource = localDataSource;
        mRemoteDataSource = remoteDataSource;
        mCurrencyRepository = currencyRepository;
    }

    public static synchronized PropertyRepository getInstance(@NonNull PropertyDataSource localDataSource,
                                                 @NonNull PropertyDataSource remoteDataSource,
                                                 @NonNull CurrencyRepository currencyRepository) {
        checkNotNull(localDataSource, "Local source cannot be null");
        checkNotNull(remoteDataSource, "Remote source cannot be null");
        checkNotNull(currencyRepository, "Currency Repository cannot be null");

        if (mInstance == null) {
            mInstance = new PropertyRepository(localDataSource, remoteDataSource, currencyRepository);
        }
        return mInstance;
    }

    @Override
    public Flowable<List<Property>> getProperties() {
        if (mCachedProperties != null && !mCacheIsDirty) {
            return Flowable.fromIterable(mCachedProperties.values()).toList().toFlowable();
        } else if (mCachedProperties == null) {
            mCachedProperties = new LinkedHashMap<>();
        }

        Flowable<List<Property>> remoteProperties = getRemoteProperties();

        if (mCacheIsDirty) {
            return remoteProperties;
        } else {
            Flowable<List<Property>> localProperties = getLocalProperties();
            return Flowable.concat(localProperties, remoteProperties)
                    .filter(properties -> !properties.isEmpty())
                    .firstOrError()
                    .toFlowable();
        }
    }

    @Override
    public Single<Property> getProperty(long id) {
        if (mCachedProperties != null && !mCacheIsDirty && mCachedProperties.get(id) != null) {
            return Single.just(mCachedProperties.get(id));
        }
        return mLocalDataSource.getProperty(id);
    }

    @Override
    public void saveProperty(Property property) {
        if (mCachedProperties == null) {
            mCachedProperties = new LinkedHashMap<>();
        }
        mCachedProperties.put(property.getId(), property);
        mLocalDataSource.saveProperty(property);
    }

    @Override
    public void forceCache(boolean value) {
        mCacheIsDirty = value;
    }

    @Override
    public void destroyInstance() {
        mInstance = null;
    }

    private Flowable<List<Property>> getLocalProperties() {
        return mLocalDataSource.getProperties()
                .flatMap(properties -> Flowable.fromIterable(properties)
                        .doOnNext(property -> mCachedProperties.put(property.getId(), property))
                        .toList()
                        .toFlowable());
    }

    private Flowable<List<Property>> getRemoteProperties() {
        return mRemoteDataSource
                .getProperties()
                .flatMap(properties -> Flowable.fromIterable(properties).doOnNext(property -> {

                    property.setLowestPricePerNight(       // base currency could be returned from PreferenceRepository with user prefereces
                            mCurrencyRepository.convert(property.getLowestPricePerNight(), BASE_CURRENCY)
                    );
                    saveProperty(property);
                    mCachedProperties.put(property.getId(), property); // updating cache
                }).toList().toFlowable())
                .doOnComplete(() -> mCacheIsDirty = false);
    }

}
