package sdejesus.hostelworld.data.remote;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import sdejesus.hostelworld.data.PropertyDataSource;
import sdejesus.hostelworld.data.model.Property;
import sdejesus.hostelworld.network.NetworkApi;
import sdejesus.hostelworld.network.reponse.PropertiesResponse;

import static com.google.common.base.Preconditions.checkNotNull;

public class PropertyRemoteDataSource implements PropertyDataSource {

    private static PropertyRemoteDataSource mInstance;

    private NetworkApi mApi;

    private PropertyRemoteDataSource(@NonNull NetworkApi networkApi) {
        mApi = networkApi;
    }

    public static PropertyRemoteDataSource getInstance(@NonNull NetworkApi networkApi) {
        checkNotNull(networkApi, "Network API cannot be null!");
        if (mInstance == null) {
            mInstance = new PropertyRemoteDataSource(networkApi);
        }
        return mInstance;
    }

    @Override
    public Flowable<List<Property>> getProperties() {
        return mApi.getProperties()
                .map(PropertiesResponse::getProperties)
                .toFlowable();
    }

    @Override
    public Single<Property> getProperty(long id) {
        return null;
    }

    @Override
    public void saveProperty(Property property) {

    }

    @Override
    public void forceCache(boolean value) {

    }


    @Override
    public void destroyInstance() {
        mInstance = null;
    }
}
