package sdejesus.hostelworld.data;

import sdejesus.hostelworld.data.model.Price;

public interface CurrencyDataSource {

    Price convert(Price current, String toCurrency);

}
