package sdejesus.hostelworld.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Facility {

    @SerializedName("id")
    private String mId;

    @SerializedName("name")
    private String mName;

    @SerializedName("facilities")
    private List<Facility> mFacilities;

    public void setId(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setFacilities(List<Facility> facilities) {
        mFacilities = facilities;
    }

    public List<Facility> getFacilities() {
        return mFacilities;
    }

    public boolean isCategory() {
        return mFacilities != null && mFacilities.size() >= 1;
    }
}

