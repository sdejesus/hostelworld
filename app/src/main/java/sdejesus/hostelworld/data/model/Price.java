package sdejesus.hostelworld.data.model;

import com.google.gson.annotations.SerializedName;

public class Price {

    @SerializedName("value")
    private String mValue;

    @SerializedName("currency")
    private String mCurrency;

    public void setValue(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }

    public void setCurrency(String currency) {
        mCurrency = currency;
    }

    public String getCurrency() {
        return mCurrency;
    }
}