package sdejesus.hostelworld.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.text.DecimalFormat;
import java.util.List;

import sdejesus.hostelworld.data.local.database.contract.PropertyPersistenceContract;
import sdejesus.hostelworld.data.local.database.converter.FacilityConverter;
import sdejesus.hostelworld.data.local.database.converter.ImageConverter;
import sdejesus.hostelworld.data.local.database.converter.LowestPricePerNightConverter;
import sdejesus.hostelworld.data.local.database.converter.OverallRatingConverter;

@Entity(tableName = PropertyPersistenceContract.PropertyEntry.TABLE_NAME)
public class Property {

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = PropertyPersistenceContract.PropertyEntry._ID)
    private long mId;

    @SerializedName("name")
    @ColumnInfo(name = PropertyPersistenceContract.PropertyEntry.COLUMN_NAME_NAME)
    private String mName;

    @SerializedName("latitude")
    @ColumnInfo(name = PropertyPersistenceContract.PropertyEntry.COLUMN_NAME_LATITUDE)
    private double mLatitude;

    @SerializedName("longitude")
    @ColumnInfo(name = PropertyPersistenceContract.PropertyEntry.COLUMN_NAME_LONGITUDE)
    private double mLongitude;

    @SerializedName("overview")
    @ColumnInfo(name = PropertyPersistenceContract.PropertyEntry.COLUMN_NAME_OVERVIEW)
    private String mOverview;

    @SerializedName("overallRating")
    @ColumnInfo(name = PropertyPersistenceContract.PropertyEntry.COLUMN_NAME_OVERALL_RATING)
    @TypeConverters({OverallRatingConverter.class})
    private OverallRating mOverallRating;

    @SerializedName("lowestPricePerNight")
    @ColumnInfo(name = PropertyPersistenceContract.PropertyEntry.COLUMN_NAME_LOWEST_PRICE_PER_NIGHT)
    @TypeConverters({LowestPricePerNightConverter.class})
    private Price mLowestPricePerNight;

    @SerializedName("images")
    @ColumnInfo(name = PropertyPersistenceContract.PropertyEntry.COLUMN_NAME_IMAGES)
    @TypeConverters({ImageConverter.class})
    private List<Image> mImages;

    @SerializedName("facilities")
    @ColumnInfo(name = PropertyPersistenceContract.PropertyEntry.COLUMN_NAME_FACILITIES)
    @TypeConverters({FacilityConverter.class})
    private List<Facility> mFacilities;

    @SerializedName("address1")
    @ColumnInfo(name = PropertyPersistenceContract.PropertyEntry.COLUMN_NAME_ADDRESS_1)
    private String mAddress1;

    @SerializedName("address2")
    @ColumnInfo(name = PropertyPersistenceContract.PropertyEntry.COLUMN_NAME_ADDRESS_2)
    private String mAddress2;

    @SerializedName("isFeatured")
    @ColumnInfo(name = PropertyPersistenceContract.PropertyEntry.COLUMN_NAME_IS_FEATURE)
    private boolean mIsFeature;


    public void setId(long id) {
        mId = id;
    }

    public long getId() {
        return mId;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setOverview(String overview) {
        mOverview = overview;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverallRating(OverallRating overallRating) {
        mOverallRating = overallRating;
    }

    public OverallRating getOverallRating() {
        return mOverallRating;
    }

    public void setAddress1(String address1) {
        mAddress1 = address1;
    }

    public String getAddress1() {
        return mAddress1;
    }

    public void setAddress2(String address2) {
        mAddress2 = address2;
    }

    public String getAddress2() {
        return mAddress2;
    }

    public void setLowestPricePerNight(Price price) {
        mLowestPricePerNight = price;
    }

    public Price getLowestPricePerNight() {
        return mLowestPricePerNight;
    }

    public void setImages(List<Image> images) {
        mImages = images;
    }

    public List<Image> getImages() {
        return mImages;
    }

    public void setFacilities(List<Facility> facilities) {
        mFacilities = facilities;
    }

    public List<Facility> getFacilities() {
        return mFacilities;
    }

    public boolean isFeature() {
        return mIsFeature;
    }

    public void setIsFeature(boolean isFeature) {
        mIsFeature = isFeature;
    }

    public String getImageUrl(Image image) {
        return String.format("http://%s%s", image.getPrefix(), image.getSuffix());
    }

    public String getAddress() {
        if (TextUtils.isEmpty(mAddress2)) {
            return mAddress1;
        }
        return String.format("%s, %s", mAddress1, mAddress2);
    }

    public String getOverallFormatted() {
        DecimalFormat format = new DecimalFormat("##.#");
        double value = getOverallRating().getOverall() / 10.0;
        return format.format(value);
    }
}
