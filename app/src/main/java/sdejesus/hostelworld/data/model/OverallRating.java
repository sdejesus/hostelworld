package sdejesus.hostelworld.data.model;

import com.google.gson.annotations.SerializedName;

public class OverallRating {

    @SerializedName("overall")
    private int mOverall;

    @SerializedName("numberOfRatings")
    private int mNumberOfRatings;

    public void setOverall(int overall) {
        mOverall = overall;
    }

    public int getOverall() {
        return mOverall;
    }

    public void setNumberOfRatings(int numberOfRatings) {
        mNumberOfRatings = numberOfRatings;
    }

    public int getNumberOfRatings() {
        return mNumberOfRatings;
    }
}
