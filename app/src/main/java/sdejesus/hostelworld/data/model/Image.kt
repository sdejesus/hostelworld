package sdejesus.hostelworld.data.model

import com.google.gson.annotations.SerializedName

data class Image(
        @SerializedName("prefix") val prefix : String,
        @SerializedName("suffix") val suffix : String
)