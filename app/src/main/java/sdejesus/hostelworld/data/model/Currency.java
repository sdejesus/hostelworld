package sdejesus.hostelworld.data.model;

import android.support.annotation.NonNull;

public class Currency {

    private String mCurrency;
    private double mRate;
    public static final String BASE_CURRENCY = "EUR"; // Just for information purpouse

    public Currency(@NonNull String currency, double rate) {
        mCurrency = currency;
        mRate = rate;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public double getRate() {
        return mRate;
    }

}
