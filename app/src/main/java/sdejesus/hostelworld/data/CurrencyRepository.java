package sdejesus.hostelworld.data;

import android.support.annotation.NonNull;

import sdejesus.hostelworld.data.model.Price;

public class CurrencyRepository implements CurrencyDataSource {

    private static CurrencyRepository mInstance;

    private CurrencyDataSource mLocalDataSource;

    private CurrencyRepository(@NonNull CurrencyDataSource localDataSource) {
        mLocalDataSource = localDataSource;
    }

    public static synchronized CurrencyRepository getInstance(@NonNull CurrencyDataSource localDataSource) {
        if (mInstance == null) {
            mInstance = new CurrencyRepository(localDataSource);
        }
        return mInstance;
    }

    @Override
    public Price convert(Price current, String toCurrency) {
        return mLocalDataSource.convert(current, toCurrency);
    }
}
