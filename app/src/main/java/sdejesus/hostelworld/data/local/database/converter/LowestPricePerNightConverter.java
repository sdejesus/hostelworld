package sdejesus.hostelworld.data.local.database.converter;

import android.arch.persistence.room.TypeConverter;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import sdejesus.hostelworld.data.model.Price;

public class LowestPricePerNightConverter {

    @TypeConverter
    public static Price toLowestPricePerNight(@NonNull String json) {
        return new Gson().fromJson(
                json,
                Price.class
        );
    }

    @TypeConverter
    public static String fromLowestPricePerNight(Price price) {
        return new Gson().toJson(price, Price.class);
    }
}
