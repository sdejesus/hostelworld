package sdejesus.hostelworld.data.local.database.converter;

import android.arch.persistence.room.TypeConverter;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import sdejesus.hostelworld.data.model.Facility;

public class FacilityConverter {
    @TypeConverter
    public static List<Facility> toFacilities(@NonNull String json) {
        return new Gson().fromJson(
                json,
                new TypeToken<List<Facility>>() { }.getType()
        );
    }

    @TypeConverter
    public static String fromImage(List<Facility> facilities) {
        return new Gson().toJson(
                facilities, new TypeToken<List<Facility>>() { }.getType());
    }
}
