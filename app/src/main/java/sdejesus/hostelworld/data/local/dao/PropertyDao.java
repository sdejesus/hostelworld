package sdejesus.hostelworld.data.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import sdejesus.hostelworld.data.model.Property;
import sdejesus.hostelworld.data.local.database.contract.PropertyPersistenceContract;

@Dao
public interface PropertyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveProperty(Property property);

    @Query("SELECT * FROM "
            + PropertyPersistenceContract.PropertyEntry.TABLE_NAME)
    Flowable<List<Property>> loadProperties();

    @Query("SELECT * FROM "
            + PropertyPersistenceContract.PropertyEntry.TABLE_NAME +
            " WHERE "
            + PropertyPersistenceContract.PropertyEntry._ID + " = :id")
    Single<Property> loadProperty(long id);

}
