package sdejesus.hostelworld.data.local.database.converter;

import android.arch.persistence.room.TypeConverter;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import sdejesus.hostelworld.data.model.OverallRating;

public class OverallRatingConverter {

    @TypeConverter
    public static OverallRating toOverallRating(@NonNull String json) {
        return new Gson().fromJson(
                json,
                OverallRating.class
        );
    }

    @TypeConverter
    public static String fromOverallRating(OverallRating overallRating) {
        return new Gson().toJson(overallRating, OverallRating.class);
    }
}
