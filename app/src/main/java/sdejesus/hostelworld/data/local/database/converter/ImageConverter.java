package sdejesus.hostelworld.data.local.database.converter;

import android.arch.persistence.room.TypeConverter;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import sdejesus.hostelworld.data.model.Image;

public class ImageConverter {

    @TypeConverter
    public static List<Image> toImage(@NonNull String json) {
        return new Gson().fromJson(
                json,
                new TypeToken<List<Image>>() { }.getType()
        );
    }

    @TypeConverter
    public static String fromImage(List<Image> images) {
        return new Gson().toJson(
                images, new TypeToken<List<Image>>() { }.getType());
    }
}