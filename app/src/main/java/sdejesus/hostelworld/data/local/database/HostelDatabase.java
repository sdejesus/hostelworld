package sdejesus.hostelworld.data.local.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import sdejesus.hostelworld.data.model.Property;
import sdejesus.hostelworld.data.local.dao.PropertyDao;

import static sdejesus.hostelworld.data.local.database.HostelDatabase.DATABASE_VERSION;

@Database(entities = {Property.class}, version = DATABASE_VERSION)
public abstract class HostelDatabase  extends RoomDatabase {

    public static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "hostel.db";

    private static HostelDatabase mInstance;

    public abstract PropertyDao propertyDao();

    public static synchronized HostelDatabase getInstance(Context context) {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(context.getApplicationContext(),
                    HostelDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration() // just for the test propose. Should be added migrations instead
                    .build();
        }
        return mInstance;
    }
}
