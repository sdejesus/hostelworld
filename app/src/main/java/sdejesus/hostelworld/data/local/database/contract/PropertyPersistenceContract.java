package sdejesus.hostelworld.data.local.database.contract;

import android.provider.BaseColumns;

public final class PropertyPersistenceContract {

    private PropertyPersistenceContract() {
    }

    public abstract static class PropertyEntry implements BaseColumns {
        public static final String TABLE_NAME = "property";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_OVERVIEW = "overview";
        public static final String COLUMN_NAME_OVERALL_RATING = "overall_rating";
        public static final String COLUMN_NAME_LOWEST_PRICE_PER_NIGHT = "lowest_price_per_night";
        public static final String COLUMN_NAME_IMAGES = "images";
        public static final String COLUMN_NAME_FACILITIES = "facilities";
        public static final String COLUMN_NAME_ADDRESS_1 = "address1";
        public static final String COLUMN_NAME_ADDRESS_2 = "address2";
        public static final String COLUMN_NAME_IS_FEATURE = "is_feature";

    }
}
