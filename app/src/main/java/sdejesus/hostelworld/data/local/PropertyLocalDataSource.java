package sdejesus.hostelworld.data.local;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import sdejesus.hostelworld.data.PropertyDataSource;
import sdejesus.hostelworld.data.local.dao.PropertyDao;
import sdejesus.hostelworld.data.model.Property;

import static com.google.common.base.Preconditions.checkNotNull;

public class PropertyLocalDataSource implements PropertyDataSource {

    private static PropertyLocalDataSource mInstance;

    private PropertyDao mDatabase;

    private PropertyLocalDataSource(@NonNull PropertyDao database) {
        mDatabase = database;
    }

    public static PropertyLocalDataSource getInstance(@NonNull PropertyDao database) {
        checkNotNull(database, "Database cannot be null");
        if (mInstance == null) {
            mInstance = new PropertyLocalDataSource(database);
        }
        return mInstance;
    }

    @Override
    public Flowable<List<Property>> getProperties() {
        return mDatabase.loadProperties();
    }

    @Override
    public Single<Property> getProperty(long id) {
        return mDatabase.loadProperty(id);
    }

    @Override
    public void saveProperty(Property property) {
        mDatabase.saveProperty(property);
    }

    @Override
    public void forceCache(boolean value) {

    }


    @Override
    public void destroyInstance() {
        mInstance = null;
    }
}
