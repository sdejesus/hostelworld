package sdejesus.hostelworld.data.local;

import sdejesus.hostelworld.data.CurrencyDataSource;
import sdejesus.hostelworld.data.model.Currency;
import sdejesus.hostelworld.data.model.Price;

import static sdejesus.hostelworld.util.Number.toOneDecimalPlace;

public class CurrencyLocalDataSource implements CurrencyDataSource {

    private static final String TAG = CurrencyLocalDataSource.class.getSimpleName();
    private static CurrencyLocalDataSource mInstance;

    private CurrencyLocalDataSource() {}

    private static final Currency[] CONVERSION_SERVICE_DATA = {
            new Currency("EUR", 1),
            new Currency("VEF", 0.132450)
    };

    public static synchronized CurrencyLocalDataSource getInstance() {
        if (mInstance == null) {
            mInstance = new CurrencyLocalDataSource();
        }
        return mInstance;
    }

    @Override
    public Price convert(Price current, String toCurrency) {
        double value = Double.parseDouble(current.getValue());
        Currency sourceCurrency = null;
        for (Currency currency : CONVERSION_SERVICE_DATA) {
            // Finding conversion to base currency
            if (currency.getCurrency().equalsIgnoreCase(current.getCurrency())) {
                sourceCurrency = currency;
                break;
            }
        }
        if (sourceCurrency == null) return null;
        value = value * sourceCurrency.getRate();
        Currency destinationCurrency = null;
        for (Currency currency : CONVERSION_SERVICE_DATA) {
            // Finding conversion to destination currency
            if (currency.getCurrency().equalsIgnoreCase(toCurrency)) {
                destinationCurrency = currency;
                break;
            }
        }
        if (destinationCurrency == null) return null;
        value = value * destinationCurrency.getRate();
        Price price = new Price();
        price.setCurrency(destinationCurrency.getCurrency());
        price.setValue(toOneDecimalPlace(value));
        return price;
    }
}
