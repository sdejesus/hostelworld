package sdejesus.hostelworld.base;

public interface BasePresenter {

    void subscribe();

    void unsubscribe();

}
