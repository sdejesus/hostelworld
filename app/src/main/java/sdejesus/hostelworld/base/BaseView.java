package sdejesus.hostelworld.base;

import android.support.annotation.NonNull;

public interface BaseView<T> {

    void setPresenter(@NonNull T presenterParameter);

}