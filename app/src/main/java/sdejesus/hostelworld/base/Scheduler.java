package sdejesus.hostelworld.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class Scheduler implements BaseSchedulerProvider {

    @Nullable
    private static Scheduler mInstance;

    private Scheduler() {
    }

    public static synchronized Scheduler getInstance() {
        if (mInstance == null) {
            mInstance = new Scheduler();
        }
        return mInstance;
    }

    @Override
    @NonNull
    public io.reactivex.Scheduler computation() {
        return Schedulers.computation();
    }

    @Override
    @NonNull
    public io.reactivex.Scheduler io() {
        return Schedulers.io();
    }

    @Override
    @NonNull
    public io.reactivex.Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }
}
