package sdejesus.hostelworld.base.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.bumptech.glide.Glide;

import sdejesus.hostelworld.R;
import sdejesus.hostelworld.data.model.Property;

public class PropertyItemView extends FrameLayout {

    Property mProperty;
    String mCurrencyFormat;

    AppCompatTextView mTitleView;
    AppCompatTextView mMinPricePerNightView;
    AppCompatTextView mRatingView;
    AppCompatTextView mAddressView;
    AppCompatTextView mFeatureHostelView;
    AppCompatImageView mImage;

    public PropertyItemView(Context context) {
        super(context);
        initView(context);
    }

    public PropertyItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public PropertyItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PropertyItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    public void initView(Context context) {

        mCurrencyFormat = context.getString(R.string.currency_format);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.property_item_view, this, true);

        mTitleView = view.findViewById(R.id.title);
        mMinPricePerNightView = view.findViewById(R.id.minPricePerNight);
        mRatingView = view.findViewById(R.id.rating);
        mImage = view.findViewById(R.id.image);
        mAddressView = view.findViewById(R.id.address);
        mFeatureHostelView = view.findViewById(R.id.feature);
    }

    public void setProperty(@NonNull Property property) {
        mProperty = property;
        populateView();
    }

    public void populateView() {

        mTitleView.setText(mProperty.getName());
        mMinPricePerNightView.setText(
                String.format(
                        mCurrencyFormat,
                        mProperty.getLowestPricePerNight().getCurrency(),
                        mProperty.getLowestPricePerNight().getValue())
        );
        mRatingView.setText(mProperty.getOverallFormatted());
        mAddressView.setText(mProperty.getAddress());
        if (mProperty.getImages() != null && mProperty.getImages().size() > 0) {
            Glide.with(getContext())
                    .load(mProperty.getImageUrl(mProperty.getImages().get(0)))
                    .into(mImage);
        }
        mFeatureHostelView.setVisibility(mProperty.isFeature() ? VISIBLE : GONE);
    }

    public AppCompatImageView getImage() {
        return mImage;
    }
}
