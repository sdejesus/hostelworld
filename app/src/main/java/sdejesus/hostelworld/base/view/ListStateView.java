package sdejesus.hostelworld.base.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import sdejesus.hostelworld.R;

public class ListStateView extends FrameLayout {

    RecyclerView mListView;
    ProgressBar mLoadingView;
    LinearLayout mNoDataView;
    LinearLayout mErrorView;

    public ListStateView(Context context) {
        super(context);
        initView(context);
    }

    public ListStateView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public ListStateView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public ListStateView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    public void initView(Context context) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_state_view, this, true);

        mNoDataView = findViewById(R.id.empty);
        mErrorView = findViewById(R.id.error);
        mListView = findViewById(R.id.list);
        mLoadingView = findViewById(R.id.loading);
    }

    public void setLoading(boolean value) {
        mLoadingView.setVisibility(value ? VISIBLE : GONE);
        mErrorView.setVisibility(value ? GONE : mErrorView.getVisibility());
        mNoDataView.setVisibility(value ? GONE : mErrorView.getVisibility());
        mListView.setVisibility(value ? GONE : mErrorView.getVisibility());
    }

    public void showError() {
        mLoadingView.setVisibility(GONE);
        mErrorView.setVisibility(VISIBLE);
        mNoDataView.setVisibility(GONE);
        mListView.setVisibility(GONE);
    }

    public void showEmpty() {
        mLoadingView.setVisibility(GONE);
        mErrorView.setVisibility(GONE);
        mNoDataView.setVisibility(VISIBLE);
        mListView.setVisibility(GONE);
    }

    public void showList() {
        mLoadingView.setVisibility(GONE);
        mErrorView.setVisibility(GONE);
        mNoDataView.setVisibility(GONE);
        mListView.setVisibility(VISIBLE);
    }

    public RecyclerView getListView() {
        return mListView;
    }
}
