package sdejesus.hostelworld.network;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import sdejesus.hostelworld.network.reponse.PropertiesResponse;

public class NetworkApi {

    private NetworkApiDefinition mServicesDefinition;
    private static NetworkApi mInstance;

    private NetworkApi(NetworkApiDefinition servicesDefinition) {
        mServicesDefinition = servicesDefinition;
    }

    public static NetworkApi getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkApi(getServices());
        }
        return mInstance;
    }

    public Single<PropertiesResponse> getProperties() {
        return mServicesDefinition.getProperties();
    }

    private static NetworkApiDefinition getServices() {

        // Display api calls on logcat
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

        Interceptor noConnectionInterceptor = chain -> {
            try {
                return chain.proceed(chain.request());
            } catch (SocketTimeoutException | UnknownHostException exception) {
                // TODO: Add a bus using JavaRx to communicate a broadcast call that there is a connection issue
            }
            throw new IOException();
        };

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new StethoInterceptor())
                .addInterceptor(noConnectionInterceptor)
                .addInterceptor(loggingInterceptor)
                .connectTimeout(3, TimeUnit.MINUTES) // After 3 minutes of timeout for an api call
                .readTimeout(3, TimeUnit.MINUTES) // user is mostly to uninstall the app thought
                .build();

        return new Retrofit.Builder()
                .baseUrl(Url.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)
                .build()
                .create(NetworkApiDefinition.class);
    }
}
