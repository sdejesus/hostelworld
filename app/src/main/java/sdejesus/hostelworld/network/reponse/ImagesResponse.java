package sdejesus.hostelworld.network.reponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import sdejesus.hostelworld.data.model.Image;

// for testing purpose
public class ImagesResponse {

    @SerializedName("images")
    List<Image> mImages;

    public List<Image> getImages() {
        return mImages;
    }
}
