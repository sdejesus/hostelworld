package sdejesus.hostelworld.network.reponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import sdejesus.hostelworld.data.model.Property;

public class PropertiesResponse {

    @SerializedName("properties")
    List<Property> mProperties;

    public List<Property> getProperties() {
        return mProperties;
    }
}
