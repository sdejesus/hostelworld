package sdejesus.hostelworld.network.reponse;

import com.google.gson.annotations.SerializedName;

import sdejesus.hostelworld.data.model.Price;

// for testing purpose
public class LowestPricePerNightResponse {

    @SerializedName("lowestPricePerNight")
    private Price mPrice;

    public Price getPrice() {
        return mPrice;
    }
}
