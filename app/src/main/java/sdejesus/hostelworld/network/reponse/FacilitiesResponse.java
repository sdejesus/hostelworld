package sdejesus.hostelworld.network.reponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import sdejesus.hostelworld.data.model.Facility;

// for testing purpose
public class FacilitiesResponse {

    @SerializedName("facilities")
    List<Facility> mFacilities;

    public List<Facility> getFacilities() {
        return mFacilities;
    }
}
