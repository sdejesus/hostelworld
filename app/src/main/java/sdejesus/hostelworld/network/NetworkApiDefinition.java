package sdejesus.hostelworld.network;

import io.reactivex.Single;
import retrofit2.http.GET;
import sdejesus.hostelworld.network.reponse.PropertiesResponse;

public interface NetworkApiDefinition {

    @GET("properties.json")
    Single<PropertiesResponse> getProperties();

}
