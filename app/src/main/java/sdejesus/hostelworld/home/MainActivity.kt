package sdejesus.hostelworld.home

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import sdejesus.hostelworld.R
import sdejesus.hostelworld.data.model.Property
import sdejesus.hostelworld.home.adapter.PropertyAdapter
import sdejesus.hostelworld.injection.Injection
import sdejesus.hostelworld.propertydetail.PropertyDetailActivity
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.AppCompatImageView
import android.view.MenuItem
import android.view.View


class MainActivity : AppCompatActivity(), MainContract.View, PropertyAdapter.PropertyListener {

    private lateinit var presenter: MainContract.Presenter
    private lateinit var propertyAdapter: PropertyAdapter
    private lateinit var optionsCompat: ActivityOptionsCompat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MainPresenter(
                this,
                Injection.providePropertyRepository(applicationContext),
                Injection.provideScheduler())

        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        propertyAdapter = PropertyAdapter(applicationContext, listOf<Property>(), this)
        state.listView.adapter = propertyAdapter
        state.listView.layoutManager = linearLayoutManager
        state.listView.setHasFixedSize(true)
    }

    override fun onResume() {
        super.onResume()
        presenter.subscribe()
    }

    override fun onPause() {
        super.onPause()
        presenter.unsubscribe()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                supportFinishAfterTransition()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setLoadingIndicator(isLoading: Boolean) {
        state.setLoading(isLoading)
    }

    override fun showProperties(properties: List<Property>) {
        propertyAdapter.updateView(properties)
        state.showList()
    }

    override fun showPropertyDetails(propertyId: Long) {
        val intent = PropertyDetailActivity.intent(applicationContext, propertyId)
        startActivity(intent, optionsCompat.toBundle());
    }

    override fun showLoadingPropertiesError() {
        state.showError()
    }

    override fun showNoProperties() {
        state.showEmpty()
    }

    override fun onPropertyClicked(property: Property, image: AppCompatImageView) {
        optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, image as View, "image")
        presenter.onPropertyDetailRequested(property)
    }

    override fun setPresenter(presenterParameter: MainContract.Presenter) {
        presenter = presenterParameter
    }
}
