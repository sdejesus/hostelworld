package sdejesus.hostelworld.home

import io.reactivex.disposables.CompositeDisposable
import sdejesus.hostelworld.base.BaseSchedulerProvider
import sdejesus.hostelworld.data.PropertyRepository
import sdejesus.hostelworld.data.model.Property

class MainPresenter(
        val mainView: MainContract.View,
        val propertiesRepository: PropertyRepository,
        val scheduler: BaseSchedulerProvider)
    : MainContract.Presenter {

    init {
        mainView.setPresenter(this)
        propertiesRepository.forceCache(true) // first time loading is being forced to be from remote this could be changed with
                                        // returning first what in cache.
    }

    private var compositeDisposable = CompositeDisposable()

    override fun onPropertyDetailRequested(property: Property) {
        mainView.showPropertyDetails(property.id)
    }

    override fun subscribe() {
        compositeDisposable.clear();
        load();
    }

    override fun unsubscribe() {
        compositeDisposable.clear()
    }

    private fun handleProperties(properties: List<Property>) {
        when {
            properties.isNotEmpty() -> mainView.showProperties(properties)
            else -> mainView.showNoProperties()
        }
    }

    fun load() {
        val subscription = propertiesRepository
                .properties
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                        {properties ->
                            mainView.setLoadingIndicator(false)
                            handleProperties(properties)
                        },{e ->
                    mainView.setLoadingIndicator(false)
                    mainView.showLoadingPropertiesError()
                })
        mainView.setLoadingIndicator(true)
        compositeDisposable.add(subscription)
    }
}