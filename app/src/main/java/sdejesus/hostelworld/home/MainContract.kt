package sdejesus.hostelworld.home

import sdejesus.hostelworld.base.BasePresenter
import sdejesus.hostelworld.base.BaseView
import sdejesus.hostelworld.data.model.Property

interface MainContract {

    interface View : BaseView<Presenter> {

        fun setLoadingIndicator(isLoading: Boolean)

        fun showProperties(properties: List<Property>)

        fun showPropertyDetails(propertyId: Long)

        fun showLoadingPropertiesError()

        fun showNoProperties()

     }

    interface Presenter : BasePresenter {

       fun onPropertyDetailRequested(property: Property)
    }
}