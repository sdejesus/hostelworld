package sdejesus.hostelworld.home.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import sdejesus.hostelworld.R;
import sdejesus.hostelworld.base.view.PropertyItemView;
import sdejesus.hostelworld.data.model.Property;

public class PropertyAdapter extends RecyclerView.Adapter<PropertyAdapter.ViewHolder> {

    private Context mContext;
    private List<Property> mProperties;
    private PropertyListener mListener;

    public PropertyAdapter(Context context, List<Property> properties, PropertyListener propertyListener) {
        mContext = context;
        mProperties = properties;
        mListener = propertyListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PropertyAdapter.ViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.item_property, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Property property = mProperties.get(holder.getAdapterPosition());
        if (property == null) return;
        holder.mPropertyView.setProperty(property);
        holder.bind(property, holder.mPropertyView.getImage(), mListener);
    }

    @Override
    public int getItemCount() {
        return mProperties.size();
    }

    public void updateView(List<Property> properties) {
        mProperties = properties;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        PropertyItemView mPropertyView;

        public ViewHolder(View v) {
            super(v);
            mPropertyView = v.findViewById(R.id.property);
        }

        public void bind(final Property item, final AppCompatImageView image, final PropertyListener itemListener) {
            if (itemListener == null) return;
            mPropertyView.setOnClickListener(v -> itemListener.onPropertyClicked(item, image));
        }
    }

    public interface PropertyListener {
        void onPropertyClicked(Property property, AppCompatImageView image);
    }
}
