package sdejesus.hostelworld.propertydetail

import io.reactivex.disposables.CompositeDisposable
import sdejesus.hostelworld.base.BaseSchedulerProvider
import sdejesus.hostelworld.data.PropertyRepository

class PropertyDetailPresenter(
        val propertyId : Long,
        val propertyDetailView: PropertyDetailContract.View,
        val propertiesRepository: PropertyRepository,
        val scheduler: BaseSchedulerProvider
) : PropertyDetailContract.Presenter {

    private var compositeDisposable = CompositeDisposable()

    init {
        propertyDetailView.setPresenter(this)
    }

    override fun subscribe() {
        compositeDisposable.clear()
        var subscription = propertiesRepository
                .getProperty(propertyId)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                        {property ->
                            propertyDetailView.showProperty(property)
                        },{e ->
                        }
                )
        compositeDisposable.add(subscription)
    }

    override fun unsubscribe() {
        compositeDisposable.clear()
    }
}