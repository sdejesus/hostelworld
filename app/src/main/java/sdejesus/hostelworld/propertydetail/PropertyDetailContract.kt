package sdejesus.hostelworld.propertydetail

import sdejesus.hostelworld.base.BasePresenter
import sdejesus.hostelworld.base.BaseView
import sdejesus.hostelworld.data.model.Property

interface PropertyDetailContract {

    interface View : BaseView<Presenter> {

        fun showProperty(property: Property)

    }

    interface Presenter : BasePresenter {
    }
}