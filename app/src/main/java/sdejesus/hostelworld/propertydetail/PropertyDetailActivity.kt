package sdejesus.hostelworld.propertydetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.activity_property_detail.*
import sdejesus.hostelworld.R
import sdejesus.hostelworld.data.model.Property
import sdejesus.hostelworld.injection.Injection
import sdejesus.hostelworld.util.Map.getMapUrlFromLocation

class PropertyDetailActivity : AppCompatActivity(), PropertyDetailContract.View {

    private lateinit var presenter: PropertyDetailContract.Presenter

    companion object {
        private const val EXTRA_ID = "EXTRA_ID"

        fun intent(caller: Context, id: Long) : Intent {
            val intent = Intent(caller, PropertyDetailActivity::class.java)
            intent.putExtra(EXTRA_ID, id)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_detail)

        val propertyId = intent.extras.getLong(EXTRA_ID)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        PropertyDetailPresenter(
                propertyId,
                this,
                Injection.providePropertyRepository(applicationContext),
                Injection.provideScheduler())
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                supportFinishAfterTransition()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        presenter.subscribe()
    }

    override fun onPause() {
        super.onPause()
        presenter.unsubscribe()
    }

    override fun showProperty(property: Property) {
        title = property.name

        if (!(property.images == null || property.images.size <= 0)) {
            Glide.with(applicationContext)
                    .load(property.getImageUrl(property.getImages().get(0)))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(image)
        }
        Glide.with(applicationContext)
                .load(getMapUrlFromLocation(property.latitude, property.longitude))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(map)
        numberPhotos.text = String.format(getString(R.string.photos_format), property.images.size -1)
        address.text = property.address
        overview.text = property.overview
        minPricePerNight.text = String.format(
                getString(R.string.currency_format),
                property.lowestPricePerNight.currency,
                property.lowestPricePerNight.value)
        rating.text = property.overallFormatted
    }

    override fun setPresenter(presenterParameter: PropertyDetailContract.Presenter) {
        presenter = presenterParameter
    }

}