package sdejesus.hostelworld;

import android.app.Application;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // Stetho.initializeWithDefaults(this); Used only to inspect the database using chrome inspect since that
        //                                      "device file explorer" -------------------------->
        //                                      doesn't always works properly, at least not on my end
    }
}
