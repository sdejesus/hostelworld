package sdejesus.hostelworld.util;

import java.text.DecimalFormat;

public class Number {

    public static String toOneDecimalPlace(double value) {
        DecimalFormat format = new DecimalFormat("##.#");
        return format.format(value);
    }
}
