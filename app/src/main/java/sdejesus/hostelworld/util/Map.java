package sdejesus.hostelworld.util;

import android.annotation.SuppressLint;

public class Map {

    @SuppressLint("DefaultLocale")
    public static String getMapUrlFromLocation(double lat, double lng) {
        String url = "http://maps.google.com/maps/api/staticmap?center=%f,%f&zoom=18&size=400x400&sensor=false";
        return String.format(url, lat, lng);
    }
}
